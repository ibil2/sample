import MeetupList from '../components/meetups/MeetupList'

function HomePage() {
    
    const dummy = [
        {
            id: 1,
            title: "First post",
            image: "https://images.pexels.com/photos/1289845/pexels-photo-1289845.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
            description:"The First Meeting",
            address:"city, state, country"
        },
        {
            id: 2,
            title: "Second post",
            image: "https://images.pexels.com/photos/1289845/pexels-photo-1289845.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
            description:"The Second Meeting",
            address:"city, state, country"
        }
    ];

    return <MeetupList meetups={dummy}/>
}

export default HomePage
